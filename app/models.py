from django.db import models
from django.contrib.auth.models import User


class ShopUser(User):
    pass


class Seller(models.Model):
    name = models.CharField(max_length=150)
    user = models.ForeignKey(ShopUser, on_delete=models.CASCADE)


class Client(models.Model):
    name = models.CharField(max_length=150)
    user = models.ForeignKey(ShopUser, on_delete=models.CASCADE)


class Author(models.Model):
    name = models.CharField(max_length=255)
    image = models.ImageField(upload_to='authors/')


class Book(models.Model):
    name = models.CharField(max_length=255)
    price = models.DecimalField(decimal_places=2,max_digits=9)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)


class BookImage(models.Model):
    image = models.ImageField(upload_to='books/')
    book = models.ForeignKey(Book, on_delete=models.CASCADE)


class Cart(models.Model):
    pass


class Order(models.Model):
    pass
